// Import Modules
import { SnwItemSheet } from "./module/item/item-sheet.js";
import { SnwActorSheetCharacter } from "./module/actor/character-sheet.js";
import { SnwActorSheetMonster } from "./module/actor/monster-sheet.js";
import { preloadHandlebarsTemplates } from "./module/preloadTemplates.js";
import { SnwActor } from "./module/actor/entity.js";
import { SnwItem } from "./module/item/entity.js";
import { SNW } from "./module/config.js";
import { registerSettings } from "./module/settings.js";
import { registerHelpers } from "./module/helpers.js";
import * as chat from "./module/chat.js";
import * as treasure from "./module/treasure.js";
import * as macros from "./module/macros.js";
import * as party from "./module/party.js";
import { SnwCombat } from "./module/combat.js";
import * as renderList from "./module/renderList.js";

/* -------------------------------------------- */
/*  Foundry VTT Initialization                  */
/* -------------------------------------------- */

Hooks.once("init", async function () {
  /**
   * Set an initiative formula for the system
   * @type {String}
   */
  CONFIG.Combat.initiative = {
    formula: "1d6 + @initiative.value",
    decimals: 2,
  };

  CONFIG.SNW = SNW;

  game.snw = {
    rollItemMacro: macros.rollItemMacro,
  };

  // Custom Handlebars helpers
  registerHelpers();

  // Register custom system settings
  registerSettings();

  CONFIG.Actor.documentClass = SnwActor;
  CONFIG.Item.documentClass = SnwItem;

  // Register sheet application classes
  Actors.unregisterSheet("core", ActorSheet);
  Actors.registerSheet("snw", SnwActorSheetCharacter, {
    types: ["character"],
    makeDefault: true,
    label: "SNW.SheetClassCharacter"
  });
  Actors.registerSheet("snw", SnwActorSheetMonster, {
    types: ["monster"],
    makeDefault: true,
    label: "SNW.SheetClassMonster"
  });

  Items.unregisterSheet("core", ItemSheet);
  Items.registerSheet("snw", SnwItemSheet, {
    makeDefault: true,
    label: "SNW.SheetClassItem"
  });

  await preloadHandlebarsTemplates();
});

/**
 * This function runs after game data has been requested and loaded from the servers, so entities exist
 */
Hooks.once("setup", function () {
  // Localize CONFIG objects once up-front
  const toLocalize = ["saves_short", "saves_long", "scores", "colors", "tags", "classes", "spell_casters", "spell_levels"];
  for (let o of toLocalize) {
    CONFIG.SNW[o] = Object.entries(CONFIG.SNW[o]).reduce((obj, e) => {
      obj[e[0]] = game.i18n.localize(e[1]);
      return obj;
    }, {});
  }

  // Custom languages
  const languages = game.settings.get("snw", "languages");
  if (languages != "") {
    const langArray = languages.split(',');
    langArray.forEach((l, i) => langArray[i] = l.trim())
    CONFIG.SNW.languages = langArray;
  }
});

Hooks.once("ready", async () => {
  Hooks.on("hotbarDrop", (bar, data, slot) =>
    macros.createSnwMacro(data, slot)
  );
});

// License and KOFI infos
Hooks.on("renderSidebarTab", async (object, html) => {
  if (object instanceof ActorDirectory) {
    party.addControl(object, html);
  }
  if (object instanceof Settings) {
    let gamesystem = html.find("#game-details");
    // SRD Link
    let snw = gamesystem.find('h4').last();
    snw.append(` <sub><a href="https://oldschoolessentials.necroticgnome.com/srd/index.php">SRD<a></sub>`);

    // License text
    const template = "systems/snw/templates/chat/license.html";
    const rendered = await renderTemplate(template);
    gamesystem.find(".system").append(rendered);

    // User guide
    let docs = html.find("button[data-action='docs']");
    const styling = "border:none;margin-right:2px;vertical-align:middle;margin-bottom:5px";
    $(`<button data-action="userguide"><img src='/systems/snw/assets/dragon.png' width='16' height='16' style='${styling}'/>Old School Guide</button>`).insertAfter(docs);
    html.find('button[data-action="userguide"]').click(ev => {
      new FrameViewer('https://gitlab.com/sisyphus192/foundry-vtt-swords-and-wizardry', { resizable: true }).render(true);
    });
  }
});

Hooks.on("preCreateCombatant", (combat, data, options, id) => {
  let init = game.settings.get("snw", "initiative");
  if (init == "group") {
    SnwCombat.addCombatant(combat, data, options, id);
  }
});

Hooks.on("updateCombatant", SnwCombat.updateCombatant);
Hooks.on("renderCombatTracker", SnwCombat.format);
Hooks.on("preUpdateCombat", SnwCombat.preUpdateCombat);
Hooks.on("getCombatTrackerEntryContext", SnwCombat.addContextEntry);

Hooks.on("renderChatLog", (app, html, data) => SnwItem.chatListeners(html));
Hooks.on("getChatLogEntryContext", chat.addChatMessageContextOptions);
Hooks.on("renderChatMessage", chat.addChatMessageButtons);
Hooks.on("renderRollTableConfig", treasure.augmentTable);
Hooks.on("updateActor", party.update);

Hooks.on("renderCompendium", renderList.RenderCompendium);
Hooks.on("renderSidebarDirectory", renderList.RenderDirectory);
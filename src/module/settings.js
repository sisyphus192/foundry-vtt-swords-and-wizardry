export const registerSettings = function () {

  game.settings.register("snw", "initiative", {
    name: game.i18n.localize("SNW.Setting.Initiative"),
    hint: game.i18n.localize("SNW.Setting.InitiativeHint"),
    default: "group",
    scope: "world",
    type: String,
    config: true,
    choices: {
      individual: "SNW.Setting.InitiativeIndividual",
      group: "SNW.Setting.InitiativeGroup",
    },
    onChange: _ => window.location.reload()
  });

  game.settings.register("snw", "rerollInitiative", {
    name: game.i18n.localize("SNW.Setting.RerollInitiative"),
    hint: game.i18n.localize("SNW.Setting.RerollInitiativeHint"),
    default: "reset",
    scope: "world",
    type: String,
    config: true,
    choices: {
      keep: "SNW.Setting.InitiativeKeep",
      reset: "SNW.Setting.InitiativeReset",
      reroll: "SNW.Setting.InitiativeReroll",
    }
  });

  game.settings.register("snw", "ascendingAC", {
    name: game.i18n.localize("SNW.Setting.AscendingAC"),
    hint: game.i18n.localize("SNW.Setting.AscendingACHint"),
    default: false,
    scope: "world",
    type: Boolean,
    config: true,
    onChange: _ => window.location.reload()
  });

  game.settings.register("snw", "morale", {
    name: game.i18n.localize("SNW.Setting.Morale"),
    hint: game.i18n.localize("SNW.Setting.MoraleHint"),
    default: false,
    scope: "world",
    type: Boolean,
    config: true,
  });

  game.settings.register("snw", "encumbranceOption", {
    name: game.i18n.localize("SNW.Setting.Encumbrance"),
    hint: game.i18n.localize("SNW.Setting.EncumbranceHint"),
    default: "detailed",
    scope: "world",
    type: String,
    config: true,
    choices: {
      disabled: "SNW.Setting.EncumbranceDisabled",
      basic: "SNW.Setting.EncumbranceBasic",
      detailed: "SNW.Setting.EncumbranceDetailed",
      complete: "SNW.Setting.EncumbranceComplete",
    },
    onChange: _ => window.location.reload()
  });

  game.settings.register("snw", "significantTreasure", {
    name: game.i18n.localize("SNW.Setting.SignificantTreasure"),
    hint: game.i18n.localize("SNW.Setting.SignificantTreasureHint"),
    default: 800,
    scope: "world",
    type: Number,
    config: true,
    onChange: _ => window.location.reload()
  });

  game.settings.register("snw", "languages", {
    name: game.i18n.localize("SNW.Setting.Languages"),
    hint: game.i18n.localize("SNW.Setting.LanguagesHint"),
    default: "",
    scope: "world",
    type: String,
    config: true,
    onChange: _ => window.location.reload()
  });
};

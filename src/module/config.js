export const SNW = {
  scores: {
    str: "SNW.scores.str.long",
    dex: "SNW.scores.dex.long",
    con: "SNW.scores.con.long",
    int: "SNW.scores.int.long",
    wis: "SNW.scores.wis.long",
    cha: "SNW.scores.cha.long",
  },
  roll_type: {
    result: "=",
    above: "≥",
    below: "≤"
  },
  alignment: {
    law: "SNW.alignments.law",
    neutral: "SNW.alignments.neutral",
    chaos: "SNW.alignments.chaos"
  },
  races: {
    human: "SNW.races.human",
    half_elf: "SNW.races.half_elf",
    elf: "SNW.races.elf",
    dwarf: "SNW.races.dwarf",
    halfling: "SNW.races.halfling",
    half_orc: "SNW.races.half_orc"
  },
  classes: {
    assassin: "SNW.classes.assassin",
    cleric: "SNW.classes.cleric",
    druid: "SNW.classes.druid",
    fighter: "SNW.classes.fighter",
    magic_user: "SNW.classes.magic_user",
    monk: "SNW.classes.monk",
    paladin: "SNW.classes.paladin",
    ranger: "SNW.classes.ranger",
    thief: "SNW.classes.thief"
  },
  spell_casters: {
    cleric: "SNW.classes.cleric",
    druid: "SNW.classes.druid",
    magic_user: "SNW.classes.magic_user"
  },
  spell_levels: {
    1: "1st",
    2: "2nd",
    3: "3rd",
    4: "4th",
    5: "5th",
    6: "6th",
    7: "7th",
    8: "8th",
    9: "9th"
  },
  saves_short: {
    death: "SNW.saves.death.short",
    wand: "SNW.saves.wand.short",
    paralysis: "SNW.saves.paralysis.short",
    breath: "SNW.saves.breath.short",
    spell: "SNW.saves.spell.short",
  },
  saves_long: {
    death: "SNW.saves.death.long",
    wand: "SNW.saves.wand.long",
    paralysis: "SNW.saves.paralysis.long",
    breath: "SNW.saves.breath.long",
    spell: "SNW.saves.spell.long",
  },
  colors: {
    green: "SNW.colors.green",
    red: "SNW.colors.red",
    yellow: "SNW.colors.yellow",
    purple: "SNW.colors.purple",
    blue: "SNW.colors.blue",
    orange: "SNW.colors.orange",
    white: "SNW.colors.white"
  },
  languages: [
    "Common",
    "Lawful",
    "Chaotic",
    "Neutral",
    "Bugbear",
    "Doppelgänger",
    "Dragon",
    "Dwarvish",
    "Elvish",
    "Gargoyle",
    "Gnoll",
    "Gnomish",
    "Goblin",
    "Halfling",
    "Harpy",
    "Hobgoblin",
    "Kobold",
    "Lizard Man",
    "Medusa",
    "Minotaur",
    "Ogre",
    "Orcish",
    "Pixie"
  ],
  tags: {
    melee: "SNW.items.Melee",
    missile: "SNW.items.Missile",
    slow: "SNW.items.Slow",
    twohanded: "SNW.items.TwoHanded",
    blunt: "SNW.items.Blunt",
    brace: "SNW.items.Brace",
    splash: "SNW.items.Splash",
    reload: "SNW.items.Reload",
    charge: "SNW.items.Charge",
  },
  tag_images: {
    melee: "/systems/snw/assets/melee.png",
    missile: "/systems/snw/assets/missile.png",
    slow: "/systems/snw/assets/slow.png",
    twohanded: "/systems/snw/assets/twohanded.png",
    blunt: "/systems/snw/assets/blunt.png",
    brace: "/systems/snw/assets/brace.png",
    splash: "/systems/snw/assets/splash.png",
    reload: "/systems/snw/assets/reload.png",
    charge: "/systems/snw/assets/charge.png",
  },
  monster_saves: {
    0: {
      d: 14,
      w: 15,
      p: 16,
      b: 17,
      s: 18
    },
    1: {
      d: 12,
      w: 13,
      p: 14,
      b: 15,
      s: 16
    },
    4: {
      d: 10,
      w: 11,
      p: 12,
      b: 13,
      s: 14
    },
    7: {
      d: 8,
      w: 9,
      p: 10,
      b: 10,
      s: 12
    },
    10: {
      d: 6,
      w: 7,
      p: 8,
      b: 8,
      s: 10
    },
    13: {
      d: 4,
      w: 5,
      p: 6,
      b: 5,
      s: 8
    },
    16: {
      d: 2,
      w: 3,
      p: 4,
      b: 3,
      s: 6
    },
    19: {
      d: 2,
      w: 2,
      p: 2,
      b: 2,
      s: 4
    },
    22: {
      d: 2,
      w: 2,
      p: 2,
      b: 2,
      s: 2
    },
  },
  monster_thac0: {
    0: 20,
    1: 19,
    2: 18,
    3: 17,
    4: 16,
    5: 15,
    6: 14,
    7: 13,
    9: 12,
    10: 11,
    12: 10,
    14: 9,
    16: 8,
    18: 7,
    20: 6,
    22: 5
  }
};
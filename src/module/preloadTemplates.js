export const preloadHandlebarsTemplates = async function () {
    const templatePaths = [
        //Character Sheets
        'systems/snw/templates/actors/character-sheet.html',
        'systems/snw/templates/actors/monster-sheet.html',
        //Actor partials
        //Sheet tabs
        'systems/snw/templates/actors/partials/character-header.html',
        'systems/snw/templates/actors/partials/character-attributes-tab.html',
        'systems/snw/templates/actors/partials/character-abilities-tab.html',
        'systems/snw/templates/actors/partials/character-spells-tab.html',
        'systems/snw/templates/actors/partials/character-inventory-tab.html',
        'systems/snw/templates/actors/partials/character-notes-tab.html',

        'systems/snw/templates/actors/partials/monster-header.html',
        'systems/snw/templates/actors/partials/monster-attributes-tab.html'
    ];
    return loadTemplates(templatePaths);
};
